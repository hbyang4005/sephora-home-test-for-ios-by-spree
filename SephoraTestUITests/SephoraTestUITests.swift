//
//  SephoraTestUITests.swift
//  SephoraTestUITests
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import XCTest

class SephoraTestUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        app = XCUIApplication()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testItemListCollectionViewLazyLoad() {
        app.launch()
        let collectionView = app.collectionViews.element(boundBy: 0)
        let lastCell = collectionView.cells.element(boundBy: 20)
        collectionView.scrollToElement(element: lastCell)
    }
    
    func testItemListCollectionViewTap() {
        app.launch()
        app.collectionViews.cells.element(boundBy: 0).tap()
        sleep(1)
        app.tables.firstMatch.swipeUp()
        let moreLessBtn = app.tables.buttons["moreLessBtn"]
        moreLessBtn.coordinate(withNormalizedOffset: CGVector(dx:0.5, dy:0.5)).tap()
        sleep(1)
        let howTo = app.tables.buttons["howTo"]
        howTo.coordinate(withNormalizedOffset: CGVector(dx:0.5, dy:0.5)).tap()
        sleep(1)
        let ingredients = app.tables.buttons["ingredients"]
        ingredients.coordinate(withNormalizedOffset: CGVector(dx:0.5, dy:0.5)).tap()
    }
    
}

extension XCUIElement {

    func scrollToElement(element: XCUIElement) {
        while element.visible() == false
        {
            let app = XCUIApplication()
            let startCoord = app.collectionViews.element.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5))
            let endCoord = startCoord.withOffset(CGVector(dx: 0.0, dy: -262));
            startCoord.press(forDuration: 0.01, thenDragTo: endCoord)
        }
    }

    func visible() -> Bool
    {
        guard self.exists && self.isHittable && !self.frame.isEmpty else
        {
            return false
        }

        return XCUIApplication().windows.element(boundBy: 0).frame.contains(self.frame)
    }

}
