//
//  ViewController.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    let screenSize = UIScreen.main.bounds
    @IBOutlet weak var colView_Item: UICollectionView!
    @IBOutlet weak var lbl_ItemTotalCount: UILabel!
    private var itemViewModel:ItemViewModel?
    private var prodViewModelList:[ProdViewModel] = []
    private var includedList:[IncludedModel] = []
    private var currentPageNum = 1
    private let pageSize = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colView_Item.register(ProdListCell.nib, forCellWithReuseIdentifier: ProdListCell.identifier)
        fetchData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func fetchData() {
        APIClient.shared.fetchItemList(platform: "mobile_app", productGroup: "bestsellers", include: "brand", pageNumber:currentPageNum,  pageSize: pageSize) { (item) in
            self.itemViewModel = item.map({ return ItemViewModel(item: $0)})
            if let itemDataList = self.itemViewModel?.itemDataList {
                for prod in itemDataList {
                    let item = ProdViewModel(prod: prod)
                    self.prodViewModelList.append(item)
                }
            }
            if let includedList = self.itemViewModel?.includedList {
                self.includedList.append(contentsOf: includedList)
            }
            self.lbl_ItemTotalCount.text = self.itemViewModel?.totalItemsText
            DispatchQueue.main.async {
                self.colView_Item.reloadData()
            }
        }
    }
    
    private func getBrandAttr(relationships:RelationShipsModel) -> IncludedAttributesModel {
        var attr = IncludedAttributesModel()
        let brand = relationships.brand
        if brand != nil {
            let brandId = brand?.data?.id
            let filtered = self.includedList.first(where: {$0.id == brandId})
            if let filteredAttr = filtered?.attributes {
                attr = filteredAttr
            }
        }
        return attr
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.prodViewModelList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProdListCell.identifier, for: indexPath) as! ProdListCell
        let prod = self.prodViewModelList[indexPath.item]
        
        if let relationShips = prod.relationships {
            let brandAttr = self.getBrandAttr(relationships: relationShips)
            cell.includedBrand = brandAttr
        }
        cell.prodAttributesViewModel = prod.prodAttributes
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let prod = self.prodViewModelList[indexPath.item]
        if let prodAttributes = prod.prodAttributes {
            let prodDetailView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "prodDetailView") as? ProdDetailViewController
            prodDetailView?.modalPresentationStyle = .fullScreen
            prodDetailView?.attrs = prodAttributes
            
            if let relationShips = prod.relationships {
                let brandAttr = self.getBrandAttr(relationships: relationShips)
                prodDetailView?.includedBrandAttr = brandAttr
            }
            self.present(prodDetailView!, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastSectionIndex = collectionView.numberOfSections - 1
        let lastItemIndex = collectionView.numberOfItems(inSection: lastSectionIndex) - 1
        
        if indexPath.section == lastSectionIndex && indexPath.item == lastItemIndex {
            if self.itemViewModel != nil {
                if let totalItems = self.itemViewModel?.meta.total_items {
                    if totalItems == self.prodViewModelList.count {
                        print("Last Page")
                        return
                    }
                }
            }
            self.currentPageNum += 1
            self.fetchData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = screenSize.width / 2
        let height = screenSize.width * 320 / 375
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
