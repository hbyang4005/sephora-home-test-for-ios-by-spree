//
//  ProdDetailViewController.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

class ProdDetailViewController: UIViewController {
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var tbl_ItemDetail: UITableView!
    var attrs:ProdAttributesViewModel!
    var includedBrandAttr:IncludedAttributesModel?
    
    var viewModel:ProdDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProdDetailViewModel(attrs: attrs)
        self.tbl_ItemDetail.dataSource = viewModel
        self.tbl_ItemDetail.register(ProdInfoCell.nib, forCellReuseIdentifier: ProdInfoCell.identifier)
        self.tbl_ItemDetail.register(ProdExplain1Cell.nib, forCellReuseIdentifier: ProdExplain1Cell.identifier)
        self.tbl_ItemDetail.register(ProdExplain2Cell.nib, forCellReuseIdentifier: ProdExplain2Cell.identifier)
        self.tbl_ItemDetail.estimatedRowHeight = 600
        self.tbl_ItemDetail.rowHeight = UITableView.automaticDimension
        self.initUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initUI() {
        if includedBrandAttr != nil {
            self.lbl_Title.text = includedBrandAttr?.name
        }
    }
    
    @IBAction func act_Close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
