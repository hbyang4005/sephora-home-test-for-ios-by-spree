//
//  ImageDetailViewController.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 13/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

class ImageDetailCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}

class ImageDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var colView_DetailImg: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    var currentIndex = 0
    var imgArr:[String]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.colView_DetailImg.register(ProdInfoImageCell.nib, forCellWithReuseIdentifier: ProdInfoImageCell.identifier)
        self.pageControl.currentPage = 0
        if let arr = self.imgArr {
            if arr.count > 1 {
                self.pageControl.numberOfPages = arr.count
            } else {
                self.pageControl.numberOfPages = 0
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if let arr = self.imgArr {
            if arr.count == 1 {
                count = 1
            } else {
                count = 500 * arr.count
            }
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProdInfoImageCell.identifier, for: indexPath) as! ProdInfoImageCell
        if let arr = self.imgArr {
            let idx = indexPath.item % arr.count;
            cell.imgView.imageFromURL(urlString: arr[idx])
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let arr = self.imgArr {
            let idx = indexPath.item % arr.count
            self.pageControl.currentPage = idx
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = CommonUtil.instance.screenSize.width
        return CGSize(width: width, height: width)
    }
    @IBAction func act_Close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
