//
//  ProdExplain2Cell.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 13/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

class ProdExplain2Cell: UITableViewCell {
    @IBOutlet weak var btn_how_to: UIButton!
    @IBOutlet weak var btn_ingredients: UIButton!
    
    @IBOutlet weak var lbl_contents: UILabel!
    @IBOutlet weak var btm_how_to: UIView!
    @IBOutlet weak var btm_ingredients: UIView!
    @IBOutlet weak var cons_how_to: NSLayoutConstraint!
    @IBOutlet weak var cons_ingredients: NSLayoutConstraint!
    @IBOutlet weak var btn_MoreLess: UIButton!
    var attrs:AttributesModel?
    
    var tapSelectedHandler: ((_ tag:Int) -> ())?
    
    var moreLessSelectedHandler: ((_ lines:Int) -> ())?
    
    var item: ProdDetailViewModelItem? {
        didSet {
            guard let item = item as? ProdExplain2ViewModelItem else {
                return
            }
            self.lbl_contents.attributedText = item.howTo
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btn_how_to.accessibilityIdentifier = "howTo"
        self.btn_ingredients.accessibilityIdentifier = "ingredients"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }


    
    // UI Action
    @IBAction func actTap(_ sender: UIButton) {
        self.tapSelectedHandler?(sender.tag)
    }
    
    @IBAction func act_MoreLess(_ sender: UIButton) {
        self.moreLessSelectedHandler?(self.lbl_contents.numberOfLines)
    }
}
