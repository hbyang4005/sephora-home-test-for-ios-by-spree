//
//  ProdExplain1Cell.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 13/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

class ProdExplain1Cell: UITableViewCell {
    @IBOutlet weak var lbl_ProdInfo: UILabel!
    @IBOutlet weak var lbl_What_It_Is: UILabel!
    @IBOutlet weak var lbl_What_It_Does_Title: UILabel!
    @IBOutlet weak var lbl_What_It_Does: UILabel!
    @IBOutlet weak var btn_MoreLess: UIButton!
    var isMore = false
    
    var moreLessSelectedHandler: ((_ isMore:Bool) -> ())?
    
    var item: ProdDetailViewModelItem? {
        didSet {
            guard let item = item as? ProdExplain1ViewModelItem else {
                return
            }
            self.lbl_What_It_Is.attributedText = item.whatItIs
            self.lbl_What_It_Is.sizeToFit()
            self.lbl_What_It_Does.attributedText = item.whatItDoes
            self.lbl_What_It_Does.sizeToFit()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lbl_What_It_Is.accessibilityIdentifier = "whatItIs"
        self.lbl_What_It_Does_Title.accessibilityIdentifier = "whatItDoesTitle"
        self.lbl_What_It_Does.accessibilityIdentifier = "whatItDoes"
        self.btn_MoreLess.accessibilityIdentifier = "moreLessBtn"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    @IBAction func act_MoreLess(_ sender: UIButton) {
        self.moreLessSelectedHandler?(!self.isMore)
    }
}
