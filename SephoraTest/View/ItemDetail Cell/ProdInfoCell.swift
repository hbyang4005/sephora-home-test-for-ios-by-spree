//
//  ProdInfoCell.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

class ProdInfoCell: UITableViewCell {
    let screenSize = UIScreen.main.bounds
    @IBOutlet weak var col_Image: UICollectionView!
    @IBOutlet weak var lbl_ProdBrand: UILabel!
    @IBOutlet weak var lbl_ProdName: UILabel!
    @IBOutlet weak var lbl_ProdPrice: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lbl_reviewCount: UILabel!
    @IBOutlet var ratingArr: [UIImageView]!
    var imgArr:[String] = []
    var parentVC: UIViewController?
    var currentIndexPath:IndexPath?
    
    // Set UI
    var item: ProdDetailViewModelItem? {
        didSet {
            guard let item = item as? ProdInfoViewModelItem else {
                return
            }
            
            lbl_ProdName.text = item.prodName
            lbl_ProdPrice.text = item.prodPrice
            
            if let images = item.prodImages {
                self.imgArr = images
            }
            
            if self.imgArr.count > 1 {
                self.pageControl.numberOfPages = self.imgArr.count
            } else {
                self.pageControl.numberOfPages = 0
            }
            self.pageControl.currentPage = 0
            
            // Rating
            for (index, imageView) in ratingArr.enumerated() {
                imageView.image = item.ratingImages[index]
            }
            
            self.lbl_reviewCount.text = item.reviewCount
            DispatchQueue.main.async {
                self.col_Image.reloadData()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.col_Image.register(ProdInfoImageCell.nib, forCellWithReuseIdentifier: ProdInfoImageCell.identifier)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

// UICollectionView Delegate
extension ProdInfoCell:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if self.imgArr.count == 1 {
            count = 1
        } else {
            count = 500 * self.imgArr.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProdInfoImageCell.identifier, for: indexPath) as! ProdInfoImageCell
        
        let idx = indexPath.item % self.imgArr.count;
        cell.imgView.imageFromURL(urlString: self.imgArr[idx])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageDetailView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "imageDetailView") as? ImageDetailViewController
        imageDetailView?.modalPresentationStyle = .fullScreen
        
        let idx = indexPath.item % self.imgArr.count;
        imageDetailView?.imgArr = self.imgArr
        imageDetailView?.currentIndex = idx
        topViewController()?.present(imageDetailView!, animated: false, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let idx = indexPath.item % self.imgArr.count
        self.pageControl.currentPage = idx
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = screenSize.width
        let height = screenSize.width * 300 / 375 + 15
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

