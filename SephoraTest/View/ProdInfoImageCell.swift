//
//  ProdInfoImageCell.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

class ProdInfoImageCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

}
