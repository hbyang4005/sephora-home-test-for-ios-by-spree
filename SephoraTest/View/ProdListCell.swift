//
//  ProdListCell.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

class ProdListCell: UICollectionViewCell {
    @IBOutlet weak var imgView_Item: UIImageView!
    @IBOutlet weak var lbl_ProdBrand: UILabel!
    @IBOutlet weak var lbl_ProdName: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    
    @IBOutlet var ratingArr: [UIImageView]!
    var includedBrand:IncludedAttributesModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgView_Item.image = nil
    }
    
    
    // Set UI
    var prodAttributesViewModel: ProdAttributesViewModel? {
        didSet {
            if let imageUrl = prodAttributesViewModel?.defaultImage {
                self.imgView_Item.imageFromURL(urlString: imageUrl)
            }
            self.lbl_ProdBrand.text = includedBrand?.name
            
            self.lbl_ProdName.text = prodAttributesViewModel?.prodName
            
            self.lbl_Price.text = prodAttributesViewModel?.price
            
            // Rating
            for (index, imageView) in ratingArr.enumerated() {
                imageView.image = prodAttributesViewModel?.ratingImages[index]
            }
        }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
