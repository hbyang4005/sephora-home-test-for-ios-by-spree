//
//  APIClient.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    static let shared = APIClient()
    // Fetch Item List
    func fetchItemList(platform: String, productGroup: String, include: String, pageNumber:Int, pageSize:Int, completion:@escaping (ItemModel?)->Void) {
        Alamofire.request(APIRouter.itemList(platform: platform, productGroup: productGroup, include: include, pageNumber: pageNumber, pageSize: pageSize)).responseJSON { (response) in
            switch response.result {
            case .success(let data):
                guard let value = data as? [String: Any] else {
                    print("Malformed data received from data")
                    completion(nil)
                    return
                }
                do {
                    let dataJSON = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let items = try JSONDecoder().decode(ItemModel.self, from: dataJSON)
                    completion(items)
                } catch {
                    print(error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil)
                break
            }
        }
    }
}
