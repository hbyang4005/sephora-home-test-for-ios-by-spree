//
//  APIRouter.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import Foundation
import Alamofire
enum APIRouter: URLRequestConvertible {
    
    case itemList(platform:String, productGroup:String, include:String, pageNumber:Int, pageSize:Int)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .itemList:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .itemList:
            return "/products"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .itemList(let platform, let productGroup, let include, let pageNumber, let pageSize):
            return [ParameterKey.platform: platform, ParameterKey.productGroup: productGroup, ParameterKey.include: include, ParameterKey.pageNumber: pageNumber, ParameterKey.pageSize: pageSize]
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try APIServer.baseURL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue("iphone", forHTTPHeaderField: HTTPHeaderField.xDeviceModel.rawValue)
        urlRequest.setValue("mobile_app", forHTTPHeaderField: HTTPHeaderField.xPlatform.rawValue)
        urlRequest.setValue("Sephora/2.0.1 (com.sephora.digital; build:7; iOS 13.2.3) Alamofire/4.7.3", forHTTPHeaderField: HTTPHeaderField.userAgent.rawValue)
        urlRequest.setValue("2.0.1", forHTTPHeaderField: HTTPHeaderField.xAppVersion.rawValue)
        urlRequest.setValue("ios", forHTTPHeaderField: HTTPHeaderField.xOSName.rawValue)
        urlRequest.setValue("en-SG", forHTTPHeaderField: HTTPHeaderField.acceptLanguage.rawValue)
        urlRequest.setValue("sg", forHTTPHeaderField: HTTPHeaderField.xSiteCountry.rawValue)
        urlRequest.setValue("mobileapp_ios", forHTTPHeaderField: HTTPHeaderField.xAppPlatform.rawValue)
        urlRequest.setValue("Gzip", forHTTPHeaderField: HTTPHeaderField.acceptEncoding.rawValue)

        // Parameters
        if let parameters = parameters {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        }
        return urlRequest
    }
}

