//
//  ProdDetailViewModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import Foundation
import UIKit

enum ProdDetailViewModelItemType {
    case prodInfo
    case prodExplain_1
    case prodExplain_2
}

protocol ProdDetailViewModelItem {
    var type: ProdDetailViewModelItemType { get }
    var rowCount: Int { get }
}

class ProdDetailViewModel: NSObject {
    var items = [ProdDetailViewModelItem]()
    
    init(attrs: ProdAttributesViewModel) {
        let prodInfoItem = ProdInfoViewModelItem(attrs: attrs)
        items.append(prodInfoItem)
        
        let prodExplain1Item = ProdExplain1ViewModelItem(attrs: attrs)
        items.append(prodExplain1Item)
        
        let prodExplain2Item = ProdExplain2ViewModelItem(attrs: attrs)
        items.append(prodExplain2Item)
    }
}


// UITableView DataSource
extension ProdDetailViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        switch item.type {
        case .prodInfo:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ProdInfoCell.identifier, for: indexPath) as? ProdInfoCell{
                cell.item = item
                return cell
            }
        case .prodExplain_1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ProdExplain1Cell.identifier, for: indexPath) as? ProdExplain1Cell{
                cell.item = item
                cell.moreLessSelectedHandler = ({ isMore in
                    tableView.beginUpdates()
                    cell.isMore = !cell.isMore
                    if isMore {
                        // Benefit
                        cell.lbl_What_It_Does.isHidden = false
                        cell.lbl_What_It_Does_Title.isHidden = false
                        cell.btn_MoreLess.setTitle("Less", for: .normal)
                    } else {
                        cell.lbl_What_It_Does.isHidden = true
                        cell.lbl_What_It_Does_Title.isHidden = true
                        cell.btn_MoreLess.setTitle("More", for: .normal)
                    }
                    
                    tableView.endUpdates()
                })
                
                return cell
            }
        case .prodExplain_2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ProdExplain2Cell.identifier, for: indexPath) as? ProdExplain2Cell{
                cell.item = item
                cell.moreLessSelectedHandler = ({ lines in
                    tableView.beginUpdates()
                    if lines == 4 {
                        cell.lbl_contents.numberOfLines = 0
                        cell.btn_MoreLess.setTitle("Less", for: .normal)
                    } else {
                        cell.lbl_contents.numberOfLines = 4
                        cell.btn_MoreLess.setTitle("More", for: .normal)
                    }
                    
                    tableView.endUpdates()
                })
                
                cell.tapSelectedHandler = ({ tag in
                    guard let item = item as? ProdExplain2ViewModelItem else {
                        return
                    }
                    tableView.beginUpdates()
                    cell.lbl_contents.numberOfLines = 4
                    cell.btn_MoreLess.setTitle("More", for: .normal)
                    // HOW TO
                    if tag == 101 {
                        cell.btn_how_to.setTitleColor(UIColor.red, for: .normal)
                        cell.btn_ingredients.setTitleColor(UIColor.lightGray, for: .normal)
                        cell.cons_how_to.constant = 5
                        cell.cons_ingredients.constant = 2
                        cell.btm_how_to.backgroundColor = UIColor.red
                        cell.btm_ingredients.backgroundColor = UIColor.lightGray
                        if let how_to = item.howTo {
                            cell.lbl_contents.attributedText = how_to
                        }
                    // INGREDIENTS
                    } else {
                        cell.btn_how_to.setTitleColor(UIColor.lightGray, for: .normal)
                        cell.btn_ingredients.setTitleColor(UIColor.red, for: .normal)
                        cell.cons_how_to.constant = 2
                        cell.cons_ingredients.constant = 5
                        cell.btm_how_to.backgroundColor = UIColor.lightGray
                        cell.btm_ingredients.backgroundColor = UIColor.red
                        if let ingredients = item.ingredients {
                            cell.lbl_contents.attributedText = ingredients
                        }
                    }
                    tableView.endUpdates()
                })
                
                return cell
            }
        }
        return UITableViewCell()
    }
}


struct ProdInfoViewModelItem: ProdDetailViewModelItem {
    var type: ProdDetailViewModelItemType {
        return .prodInfo
    }
    var rowCount: Int {
        return 1
    }
    var prodName: String?
    var prodPrice: String?
    var reviewCount: String?
    var ratingImages: [UIImage] = []
    var prodImages: [String]?
    
    init(attrs: ProdAttributesViewModel) {
        self.prodName = attrs.prodDetailName
        self.prodPrice = attrs.price
        self.reviewCount = attrs.reviewsCount
        self.ratingImages = attrs.ratingImages
        self.prodImages = attrs.prodDetailImages
    }
}

struct ProdExplain1ViewModelItem: ProdDetailViewModelItem {
    var type: ProdDetailViewModelItemType {
        return .prodExplain_1
    }
    var rowCount: Int {
        return 1
    }

    var whatItIs: NSAttributedString?
    var whatItDoes: NSAttributedString?
//    var isOpened: Bool
    var moreBtnTitle: String
    init(attrs:ProdAttributesViewModel) {
        self.whatItIs = attrs.whatItIs
        self.whatItDoes = attrs.whatItDoes
//        if self.isOpened {
//            self.moreBtnTitle = "Less"
//        } else {
//            self.moreBtnTitle = "More"
//        }
        self.moreBtnTitle = "More"
    }
}

struct ProdExplain2ViewModelItem: ProdDetailViewModelItem {
    var type: ProdDetailViewModelItemType {
        return .prodExplain_2
    }
    var rowCount: Int {
        return 1
    }

    var howTo: NSAttributedString?
    var ingredients: NSAttributedString?
    var moreBtnTitle: String
    init(attrs:ProdAttributesViewModel) {
        self.howTo = attrs.howTo
        self.ingredients = attrs.ingredients
        self.moreBtnTitle = "More"
    }
}
