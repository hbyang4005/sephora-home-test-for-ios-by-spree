//
//  ItemViewModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import Foundation

struct ItemViewModel {
    var itemDataList:[ProdModel]
    var includedList:[IncludedModel]
    var meta:MetaModel
    var links:LinksModel
    let totalItemsText:String
    
    init(item: ItemModel) {
        self.itemDataList = item.data
        self.includedList = item.included
        self.totalItemsText = "\(String(describing: item.meta.total_items)) items found"
        self.meta = item.meta
        self.links = item.links
    }
}
