//
//  ProdAttributesViewModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/23.
//  Copyright © 2020 spree. All rights reserved.
//

import UIKit

struct ProdAttributesViewModel {
    var defaultImage:String = ""
    var prodDetailImages:[String] = []
    let prodName:String?
    let prodDetailName:String?
    var price:String = ""
    var ratingImages:[UIImage] = []
    var reviewsCount:String = ""
    
    var whatItIs = NSAttributedString(string: "")
    var whatItDoes = NSAttributedString(string: "")
    
    var howTo = NSAttributedString(string: "")
    var ingredients = NSAttributedString(string: "")
    
    init(attrs: AttributesModel?) {
        
        if let images = attrs?.image_urls {
            self.prodDetailImages = images
        }
        if let defaultImageUrls = attrs?.default_image_urls {
            if defaultImageUrls.count > 0 {
                self.defaultImage = defaultImageUrls[0]
            }
        }
        // ProdName
        self.prodName = attrs?.name
        
        // Prod detail name
        var prodNameResult = attrs?.name ?? ""
        if let heading = attrs?.heading {
            prodNameResult += " ∙ \(heading)"
        }
        self.prodDetailName = prodNameResult
        
        // ProdPrice
        self.price = "₩\(attrs?.price?.withCommas() ?? "")"
        
        // Rating
        if let rating = attrs?.rating {
            var imageName = ""
            for i in 0..<5 {
                if rating >= Float(i + 1) {
                    imageName = "rating_on"
                }
                else if rating > Float(i) {
                    imageName = "rating_half"
                }
                else {
                    imageName = "rating_off"
                }
                if let image = UIImage(named: imageName) {
                    self.ratingImages.append(image)
                }
            }
        }
        // Reviews Count
        self.reviewsCount = "\(attrs?.reviews_count ?? 0) Reviews"
        let style = "<style>body{font-family: 'SephoraSans-Book'; font-size:14px;}ol { display: block; list-style-type: none; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0; padding-left: 40px;}li { list-style-type: none; }</style>"
        if let whatItIs = attrs?.desc {
            if let attrString = (style + whatItIs).attributedHtmlString {
                self.whatItIs = attrString
            }
        }
        if let whatItDoes = attrs?.benefits {
            if let attrString = (style + whatItDoes).attributedHtmlString {
                self.whatItDoes = attrString
            }
        }
        if let howTo = attrs?.how_to_text {
            if let attrString = (style + howTo).attributedHtmlString {
                self.howTo = attrString
            }
        }
        if let ingredients = attrs?.ingredients {
            if let attrString = (style + ingredients).attributedHtmlString {
                self.ingredients = attrString
            }
        }
    }
}
