//
//  ProdViewModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/23.
//  Copyright © 2020 spree. All rights reserved.
//

import UIKit

struct ProdViewModel {
    let type:String?
    let id:String?
    let prodAttributes:ProdAttributesViewModel?
    let relationships:RelationShipsModel?
    
    init(prod:ProdModel) {
        self.type = prod.type
        self.id = prod.type
        self.prodAttributes = ProdAttributesViewModel(attrs: prod.attributes)
        self.relationships = prod.relationships
    }
}




