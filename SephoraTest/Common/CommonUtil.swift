//
//  CommonUtil.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

enum TableViewColumn: Int {
    case item_image = 0,
    item_info,
    item_info2
}

class CommonUtil {
    static var instance: CommonUtil = CommonUtil()
    var indicatorView: UIView!
    let screenSize = UIScreen.main.bounds
    private init() {
        
    }
    
    //Start Indicator
    func startIndicatorView(){
        if (indicatorView != nil) {
            indicatorView.removeFromSuperview()
            indicatorView = nil
        }
        stopIndicatiorView()
                
        if let windowView = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            indicatorView = UIView.init(frame: windowView.frame)
            
            indicatorView.backgroundColor = UIColor.clear
            
            
            let activator = UIActivityIndicatorView()
            if #available(iOS 13.0, *) {
                activator.style = .large
            } else {
                activator.style = .gray
            }
            activator.center = CGPoint.init(x: windowView.frame.size.width / 2, y: windowView.frame.size.height / 2)
            
            activator.startAnimating()
            indicatorView.addSubview(activator)
            
            windowView.addSubview(indicatorView)
        }
        

        
    }
    //Stop Indicator
    func stopIndicatiorView(){
        if (indicatorView != nil) {
            indicatorView.isHidden = true
            indicatorView.removeFromSuperview()
            indicatorView = nil
        }
    }
    
}




extension UIImageView {
    func imageFromURL(urlString:String) {
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
               if error != nil {
                  print(error!)
                  return
               }
                if let imageData = data {
                    DispatchQueue.main.async {
                        self.image = UIImage(data: imageData)
                    }
                }
            }).resume()
        }
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

extension String {
        var utfData: Data {
            return Data(utf8)
        }

        var attributedHtmlString: NSAttributedString? {

            do {
                return try NSAttributedString(data: utfData,
                options: [
                          .documentType: NSAttributedString.DocumentType.html,
                          .characterEncoding: String.Encoding.utf8.rawValue,
                         ], documentAttributes: nil)
            } catch {
                print("Error:", error)
                return nil
            }
        }
}

func topViewController() -> UIViewController? {

    if let keyWindow = UIApplication.shared.keyWindow {

        if var viewController = keyWindow.rootViewController {

            while viewController.presentedViewController != nil {

                viewController = viewController.presentedViewController!

            }

            print("topViewController -> \(String(describing: viewController))")

            return viewController

        }

    }

    return nil

}
