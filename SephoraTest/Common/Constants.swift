//
//  Constants.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import Foundation

struct APIServer {
    static let baseURL = "https://api.sephora.sg/api/v2.5"
}

struct ParameterKey {
    static let platform = "filter[platform]"
    static let productGroup = "filter[product_group]"
    static let include = "include"
    static let pageNumber = "page[number]"
    static let pageSize = "page[size]"
    
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case xDeviceModel = "X-Device-Model"
    case xPlatform = "X-Platform"
    case userAgent = "User-Agent"
    case xAppVersion = "X-App-Version"
    case xOSName = "X-OS-Name"
    case acceptLanguage = "Accept-Language"
    case xSiteCountry = "X-Site-Country"
    case xAppPlatform = "X-App-Platform"
    case acceptEncoding = "Accept-Encoding"
    
}
