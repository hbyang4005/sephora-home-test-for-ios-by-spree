//
//  LinksModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 13/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

struct LinksModel: Codable {
    let self_url:String?
    let prev_url:String?
    let next_url:String?
    let last_url:String?
}

extension LinksModel {
    enum CodingKeys: String, CodingKey {
        case self_url = "self"
        case prev_url = "prev"
        case next_url = "next"
        case last_url = "last"
    }
}
