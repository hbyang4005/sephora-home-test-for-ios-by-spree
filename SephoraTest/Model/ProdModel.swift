//
//  ProdModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import Foundation

class ProdModel: Codable {
    let type:String?
    let id:String?
    let attributes:AttributesModel?
    let relationships:RelationShipsModel?
}
