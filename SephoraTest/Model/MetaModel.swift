//
//  MetaModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import UIKit

struct MetaModel: Codable {
    let total:Int
    let total_pages:Int
    let total_items:Int
    let current_page:Int
    let per_page:Int
}

extension MetaModel {
    enum CodingKeys: String, CodingKey {
        case total
        case total_pages = "total-pages"
        case total_items = "total-items"
        case current_page = "current-page"
        case per_page = "per-page"
    }
}
