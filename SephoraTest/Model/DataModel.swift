//
//  DataModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/23.
//  Copyright © 2020 spree. All rights reserved.
//

import UIKit

class DataModel: Codable {
    let type:String?
    let id:String?
}
