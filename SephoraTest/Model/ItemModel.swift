//
//  ItemModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import Foundation
struct ItemModel: Codable {
    let data:[ProdModel]
    let meta:MetaModel
    let links:LinksModel
    let included:[IncludedModel]
}
