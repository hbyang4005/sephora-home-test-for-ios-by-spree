//
//  AttributesModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 12/12/2019.
//  Copyright © 2019 spree. All rights reserved.
//

import Foundation

struct AttributesModel: Codable {
    let name:String?
    let heading:String?
    let rating:Float?
    let how_to_text:String?
    let desc:String?
    let benefits:String?
    let application:String?
    let ingredients:String?
    let reviews_count:Int?
    let image_urls:[String]?
    let default_image_urls:[String]?
    let price:Int?
    let original_price:Int?
    // Included
    let slug_url:String?
}

extension AttributesModel {
    enum CodingKeys: String, CodingKey {
        case name
        case heading
        case rating
        case how_to_text = "how-to-text"
        case desc = "description"
        case benefits
        case application
        case ingredients
        case reviews_count = "reviews-count"
        case image_urls = "image-urls"
        case default_image_urls = "default-image-urls"
        case price
        case original_price
        case slug_url = "slug-url"
    }
}
