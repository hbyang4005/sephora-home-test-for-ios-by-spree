//
//  IncludedAttributesModel.swift
//  SephoraTest
//
//  Created by HongBeom Yang on 2020/01/23.
//  Copyright © 2020 spree. All rights reserved.
//

import UIKit

struct IncludedAttributesModel: Codable {
    let name:String?
    let brandLimit:String?
    let mobileAppBannerImage:String?
    let slugUrl:String?
    
    init() {
        self.name = ""
        self.brandLimit = ""
        self.mobileAppBannerImage = ""
        self.slugUrl = ""
    }
}

extension IncludedAttributesModel {
    enum CodingKeys: String, CodingKey {
        case name
        case brandLimit = "brand-limit"
        case mobileAppBannerImage = "mobile-app-banner-image"
        case slugUrl = "slug-url"
    }
}

