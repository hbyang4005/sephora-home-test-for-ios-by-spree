//
//  APITests.swift
//  SephoraTestTests
//
//  Created by HongBeom Yang on 2020/01/30.
//  Copyright © 2020 spree. All rights reserved.
//

import XCTest
@testable import SephoraTest

class APITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testRequestItemList() {
        let e = expectation(description: "Expecting a JSON data not nil")
        APIClient.shared.fetchItemList(platform: "mobile_app", productGroup: "bestsellers", include: "brand", pageNumber:1,  pageSize: 30) { (item) in
            let itemViewModel = item.map({ return ItemViewModel(item: $0)})
            let prodList = itemViewModel?.itemDataList
            XCTAssert(prodList!.count > 0, "Item list is not present")
            e.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
