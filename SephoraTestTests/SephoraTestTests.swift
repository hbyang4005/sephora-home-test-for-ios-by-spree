//
//  SephoraTestTests.swift
//  SephoraTestTests
//
//  Created by HongBeom Yang on 2020/01/21.
//  Copyright © 2020 spree. All rights reserved.
//

import XCTest
@testable import SephoraTest

class SephoraTestTests: XCTestCase {
    var apiClient:APIClient!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        apiClient = APIClient()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
}
